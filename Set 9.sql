Task 1:
DDL operations:
Drop the "projects" table.
Solution:
DROP TABLE projects;

Task 2:
Single Function (Number):
Display the commission earned by each employee in the "employees" table, assuming their commission_pct represents the portion of 
their salary they receive as a commission.
Solution:
SELECT
      employee_id,
      first_name,
      last_name,
      (salary*commission_pct) AS commission_earned
FROM employees;

Task 3:
Subqueries:
Write a query to find all employees whose manager earns more than the average salary of all managers in the "employees" table.
Solution:
SELECT
    employee_id,
    first_name,
    last_name
FROM employees
WHERE salary>(
    SELECT AVG(salary)
    FROM employees
    WHERE manager_id IS NOT NULL);

Task 4:
Set Operators:
Use the INTERSECT operator to find all department_ids that are present in both the "employees" and "departments" tables.
Solution:
SELECT department_id FROM employees
INTERSECT
SELECT department_id FROM departments;

Task 5:
Joins:
Write a query that joins the "employees" table and the "departments" table and displays the employee's first name, last name,
and department name, only for those departments that have more than 50 employees.
Solution:
SELECT 
     e.first_name,
     e.last_name,
     d.department_name
FROM employees e
INNER JOIN departments d
ON e.department_id=d.department_id
GROUP BY
      e.first_name,
      e.last_name,
      d.department_name
HAVING COUNT(e.employee_id)>50;
