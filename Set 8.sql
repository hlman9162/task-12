Task 1:
SELECT statement:
Select all fields from the "employees" table for employees whose first name contains the letter 'e' at least twice.
Solution:
SELECT*FROM employees WHERE first_name LIKE'%e%e%';

Task 2:
Joins:
Write a query that RIGHT JOINs the "employees" table with the "departments" table and displays the employee's first name, last name, and department name.
Solution:
SELECT e.first_name,
       e.last_name,
       d.department_name
FROM employees e
RIGHT JOIN departments d
ON e.department_id=d.department_id;
    
Task 3:
DDL operations:
Rename the "employee_projects" table to "projects".
Solution:
ALTER TABLE employees_projects RENAME TO projects;

Task 4:
Aggregate Functions:
For each job_id in the "employees" table, find the difference between the maximum and minimum salaries.
Solution:
SELECT job_id,
       (MAX(salary)-MIN(salary)) AS difference_salary
FROM employees
GROUP BY job_id;

Task 5:
DML operations (INSERT):
Insert a new record into the "projects" table. Use your own discretion for the values.
Solution:
INSERT INTO projects VALUES(1,208,'01/01/2020','BANK_CORE',12);

