Task 1:
Conversion Functions: 
Show the job_id in the "jobs" table in all uppercase.
Solution:
SELECT UPPER(job_id) FROM jobs;

Task 2:
Subqueries:
Find all employees in the "employees" table whose salary is above the average salary of their respective departments.
Solution:
SELECT*FROM employees e
WHERE salary > (SELECT AVG(salary) FROM employees WHERE department_id = e.department_id);

Task 3:
Set Operators:
Use the UNION operator to create a list of all unique job_ids from both the "employees" and "job_history" tables.
Solution:
SELECT job_id FROM employees
UNION
SELECT job_id FROM job_history;

Task 4:
DML operations (DELETE):
Delete all records from the "employees_backup" table where the salary is below 3000.
Solution:
DELETE FROM employees_backup WHERE salary<3000;

Task 5:
DDL operations: 
Add a new column to the "employees_backup" table called "performance_score" of type INTEGER.
Solution:
ALTER TABLE employees_backup 
ADD performance_score number;
