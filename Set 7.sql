Task 1:
DML operations (DELETE): 
Delete all employees in the "employees_backup" table who were hired in the second half of the year (July to December).
Solution:
DELETE FROM employees_backup
WHERE EXTRACT(MONTH FROM hire_date) BETWEEN 7 AND 12;

Task 2:
Set Operators: 
Use the UNION ALL operator to list all country_id values from both the "locations" and "countries" tables.
Solution:
SELECT country_id FROM locations
UNION ALL
SELECT country_id FROM countries;

Task 3:
Single Function (Analytical):
Display the dense rank of employees based on salary within their department in the "employees" table.
Solution:
SELECT  DENSE_RANK() OVER(PARTITION BY department_id ORDER BY salary DESC)AS rank_salary FROM employees;

Task 4:
Conversion Functions:
Display the length of service for each employee in the "employees" table in years and months.

SELECT 
    employee_id,
    first_name,
    last_name,
    hire_date,
    TRUNC(MONTHS_BETWEEN(SYSDATE,hire_date) /12) AS year_length,
    TRUNC(MOD(MONTHS_BETWEEN(SYSDATE,hire_date),12)) AS month_length
FROM employees;
    
Task 5:
Subqueries:
Find the job title of the employee who earns the second highest salary in the "employees" table.
Solution:
SELECT job_id FROM (
SELECT 
         job_id,
         salary,
         DENSE_RANK() OVER(ORDER BY salary DESC) AS salary_rank
FROM employees)
WHERE salary_rank=2
;
