Task 1:
SELECT statement
Select the last_name, job_id, and department_id from the "employees" table where the last_name like 'S%'.
Solution:
SELECT last_name,job_id,department_id FROM employees WHERE last_name LIKE 'S%';

Task 2:
Single Function (Analytical):
Use the RANK() function to display employees' salary ranking within their department.
Solution:
SELECT employee_id,
       first_name,
       last_name,
       salary,
       department_id,
       RANK()OVER(PARTITION BY department_id ORDER BY SALARY) AS rank_salary
FROM employees;

Task 3:
Aggregate Functions:
Find the highest, lowest, and average number of employees in each job from the "employees" table.
Solution:
SELECT job_id,
       MAX(salary),
       MIN(salary),
       AVG(salary) 
FROM employees
GROUP BY job_id;

Task 4:
Joins:
Write a query that FULL OUTER JOINs the "employees" table with the "departments" table and displays the employee's first name,
last name, and department name.
Solution:
SELECT e.first_name,e.last_name,d.department_name
FROM employees e
FULL OUTER JOIN departments d
ON e.department_id=d.department_id;

Task 5:
Subqueries: 
Write a query that selects all employees in the "employees" table who work in a department with more than 80 employees.
SELECT*FROM employees 
WHERE department_id IN (SELECT department_id
          FROM employees
          Group by department_id
          HAVING COUNT(*)>80);
