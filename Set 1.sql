Task 1:
DDL operations
Create a copy of the "employees" table named "employees_backup" without including the data.
Solution:
CREATE TABLE employees_backup
AS
SELECT*FROM employees 
WHERE 1=0;

Task 2:
DML operations (UPDATE)
Update the salary of all employees in the "employees_backup" table by adding a 15% raise for those who were hired in the year 2000.
Solution:
DROP TABLE employees_backup;
CREATE TABLE employees_backup 
AS
SELECT*FROM employees;
SELECT*FROM employees_backup;
UPDATE employees_backup 
SET salary=salary*1.15
WHERE EXTRACT(YEAR FROM hire_date)=2000;

Task 3:
Single Function (Date)
List all employees in the "employees" table who were hired on a Friday.
Solution:
SELECT hire_date FROM employees;
SELECT*FROM employees WHERE EXTRACT(MONTH FROM hire_date)=02;

Task 4:
Aggregate Functions
Calculate the total salary paid per job in the "employees" table.
Solution:
SELECT job_id,
       SUM(salary)
FROM employees
GROUP BY job_id;

Task 5:
Joins
Using a LEFT JOIN, list all departments from the "departments" table and the count of employees in each department from the "employees" table.
Solution:
SELECT d.department_name, COUNT(e.employee_id) AS employee_count
FROM departments d
LEFT JOIN employees e ON d.department_id = e.department_id
GROUP BY d.department_name;

