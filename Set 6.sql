Task 1:
Aggregate Functions:
Find the mode of the salary (most frequently occurring salary) in the "employees" table.
Solution:
SELECT salary,COUNT(*) AS count_salary
FROM employees
GROUP BY salary
ORDER BY COUNT(*) DESC
FETCH FIRST 1 ROWS ONLY;

Task 2:
Subqueries:
Display all employees who report to the manager who manages the most employees in the "employees" table.
SELECT*FROM employees 
WHERE manager_id IN(
SELECT manager_id
FROM employees
GROUP BY manager_id
ORDER BY COUNT(employee_id) DESC
FETCH FIRST 1 ROWS ONLY);

Task 3:
Single Function (Character):
Use the CONCAT function to display each employee's full name (first name and last name) in the "employees" table.
Solution:
SELECT first_name||' '||last_name AS full_name
FROM employees;

Task 4:
Joins:
Write a query to display the first_name, last_ name, and department_name of all employees
who work in countries that have a country_name starting with 'U'.
Solution:
SELECT e.first_name,
       e.last_name,
       d.department_id,
       c.country_name
FROM employees e
INNER JOIN departments d ON e.department_id=d.department_id
INNER JOIN locations l   ON d.location_id=l.location_id
INNER JOIN countries c   ON l.country_id=c.country_id
WHERE
    c.country_name LIKE'U%';
    
Task 5:
DDL operations: 
Add a "country_id" column to the "employee_projects" table.
Solution:
ALTER TABLE employees_projects ADD country_id number;
