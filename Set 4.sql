Task 1:
DDL operations:
Alter the "employees_backup" table by dropping the "performance_score" column.
Solution:
ALTER TABLE employees_backup
DROP COLUMN performance_score;

Task 2:
DML operations (INSERT):
Insert a new record into the "employees_backup" table. Use your own discretion for the values.
Solution:
SELECT*FROM employees_backup;
INSERT INTO employees_backup 
     VALUES(207,'Leman','Huseynli','NJEHGJK','123.456.9876','18/07/0404','IT PROG',20000,NULL,203,70,NULL);

Task 3:
Single Function (Number):
Use the ROUND function to round the salary of all employees to the nearest hundred in the "employees" table.
Solution:
SELECT ROUND(salary,-2) FROM employees;

Task 4:
Aggregate Functions:
Find the department with the highest number of employees in the "employees" table.
Solution:
SELECT department_id,
       COUNT(employee_id) AS emp_number
FROM employees
GROUP BY department_id
ORDER BY emp_number DESC
FETCH FIRST  1 ROW ONLY;
       
Task 5:
Conversion Functions:
Display the hire_date of all employees in the "employees" table in the format 'YYYY-Q' where Q represents the quarter of the year.
Solution:
SELECT 
    TO_CHAR(hire_date, 'YYYY-Q') AS hire_quarter
FROM 
    employees;
