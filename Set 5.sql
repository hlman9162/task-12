Task 1:
Set Operators:
Use the MINUS operator to display all employees in the "employees" table who have never been a manager (based on the manager_id column).
Solution:
SELECT employee_id FROM employees
MINUS
SELECT manager_id FROM employees;

Task 2:
Joins:
Write a query that INNER JOINs the "employees" table with the "jobs" table and displays the employee's first name, last name, and job title.
Solution:
SELECT e.first_name,e.last_name,j.job_title
FROM employees e
INNER JOIN jobs j
ON e.job_id=j.job_id;

Task 3:
SELECT statement:
Select the first_name, last_name, salary, and department_id from the "employees" table where the salary is not in the top 5 salaries.
SELECT first_name,
       last_name,
       department_id
FROM employees
WHERE salary NOT IN  (
       SELECT salary
       FROM employees
       ORDER BY salary
       FETCH FIRST 5 ROWS ONLY);

Task 4:
DDL operations:
Create a new table called "employee_projects" that includes the following columns: "project_id" (integer), 
"employee_id" (integer), "start_date" (date), "end_date" (date), "role" (varchar).
Solution:
CREATE TABLE employees_projects(
 project_id number,
 employee_id number,
 start_date date,
 role varchar2(100));

Task 5:
DML operations (UPDATE):
In the "employees_backup" table, set the commission_pct to 0 for all employees who don't have a commission_pct.
Solution:
UPDATE employees_backup
SET commission_pct=0
WHERE commission_pct IS NULL;
