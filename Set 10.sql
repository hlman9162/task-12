Task 1:
DML operations (UPDATE): 
Update the "employees_backup" table by giving a 10% salary increase to those employees whose job_id is 'SA_REP' and who were hired after the year 2005.
Solution:
UPDATE employees_backup
SET salary=salary*1.1
WHERE job_id='SA_REP' AND 
      EXTRACT(YEAR FROM hire_date)>2005;
      
Task 2:
Aggregate Functions:
Find the median salary for the 'IT_PROG' job in the "employees" table.
Solution:
SELECT 
    MEDIAN(salary)
FROM employees
WHERE job_id='IT_PROG';

Task 3:
Single Function (Date): 
List the employees in the "employees" table who have been hired in the last 365 days.
Solution:
SELECT
    first_name,
    last_name
FROM employees
WHERE EXTRACT(YEAR FROM hire_date)>(EXTRACT(YEAR FROM hire_date)-1);

Task 4:
Conversion Functions:
Convert the hire_date in the "employees" table to 'Day, DD-Month-YYYY' format.
Solution:
SELECT TO_CHAR(hire_date,'Day, DD-Month-YYYY') FROM employees;

Task 5:
SELECT statement:
Display the employees' last names and the length of the names from the "employees" table. Order the result by the length of the names in descending order.
Solution:
SELECT 
     last_name,
     length(first_name)
FROM employees
ORDER BY length(first_name) DESC;
